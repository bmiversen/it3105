import tensorflow as tf
import numpy as np
import tflowtools as TFT

class NeuralNet:
    def __init__(self, dims, haf, oaf, cfunc, lr, iwr, opt, ds, cfrac, vfrac, vint, tfrac, mbs,
    steps, mlay, mden, dweights, dbiases):
        self.net_dims = dims
        self.hidden_function = haf
        self.output_function = oaf
        self.cost_function = cfunc
        self.learning_rate = lr
        self.initial_weight_range = iwr
        self.optimizer = opt
        self.data_source = ds
        self.case_fraction = cfrac
        self.validation_fraction = vfrac
        self.validation_interval = vint
        self.test_fraction = tfrac
        self.minibatch_size = mbs
        self.steps = steps
        self.map_layers = mlay
        self.map_dendrograms = mden
        self.display_weights = dweights
        self.display_biases = dbiases
        self.hidden_layers = []

        self.build()

    def make_cases(self):
        if self.data_source[0] == 1:
            length = int(self.data_source[1])
            cases = np.array(TFT.gen_all_parity_cases(length))
            print(cases)
            self.input_features = cases[:, :len(cases[0])]
            self.target_cases = cases[:, -1]
            print("length", len(self.input_features))

    def get_hidden_function(self):
        if self.hidden_function.lower() == "sigmoid":
            return tf.sigmoid

    def get_output_function(self):
        if self.output_function.lower() == "sigmoid":
            return tf.sigmoid

    def get_optimizer(self):
        if self.optimizer.lower() == "gradientdescent":
            return tf.train.GradientDescentOptimizer(self.learning_rate)

    def get_error(self, output, target):
        if self.cost_function.lower() == "mse":
            return tf.reduce_mean(tf.square(output - target))

    # Builds the computation graph
    def build(self):
        num_inputs = self.net_dims[1]
        self.inputs = tf.placeholder(tf.float64, shape=(None, num_inputs), name='Input')
        invar = self.inputs
        insize = num_inputs
        # Build layers
        for i in range(2, len(self.net_dims)-1):
            outsize = self.net_dims[i]
            self.build_hidden_layer(invar, insize, outsize, i-1)
            invar = self.hidden_layers[-1]
            insize = outsize
        self.output = self.build_output_layer(invar, insize, self.net_dims[-1])
        self.target = tf.placeholder(tf.float64, shape=(None, self.net_dims[-1]), name="Target")
        self.error = self.get_error(self.output, self.target)
        self.optimizer = self.get_optimizer()
        self.trainer = self.optimizer.minimize(loss=self.error)

        self.session = tf.Session()
        self.session.run(tf.global_variables_initializer())
        self.make_cases()

    def build_hidden_layer(self, invar, insize, outsize, index):
        activation = self.get_hidden_function()
        range1 = self.initial_weight_range[0]
        range2 = self.initial_weight_range[1]
        weights = tf.Variable(np.random.uniform(range1, range2,size=(insize,outsize)))
        bias = tf.Variable(np.random.uniform(range1, range2,size=(1,outsize)))
        output = activation(tf.matmul(invar, weights)+bias, name="Hidden-"+str(index))
        self.hidden_layers.append(output)

    def build_output_layer(self, invar, insize, outsize):
        activation = self.get_output_function()
        range1 = self.initial_weight_range[0]
        range2 = self.initial_weight_range[1]
        weights = tf.Variable(np.random.uniform(range1, range2,size=(insize,outsize)))
        bias = tf.Variable(np.random.uniform(range1, range2,size=(1,outsize)))
        return activation(tf.matmul(invar, weights)+bias, name="Output")

    def run_one_step(self, operators, grabbed_vars = None, dir='probeview', feed_dict=None):
        results = self.session.run([self.trainer,self.error, operators,grabbed_vars],feed_dict=feed_dict)
        return results




def test_build():
    dims = [3, 10, 2,  2]
    haf = "sigmoid"
    oaf = "sigmoid"
    cfunc = "mse"
    lr = 0.5
    iwr = [-1, 1]
    opt = "gradientdescent"
    ds = [1, 10]
    cfrac = None
    vfrac = None
    vint = None
    tfrac = None
    mbs = None
    steps = None
    mlay = None
    mden = None
    dweights = None
    dbiases = None
    net = NeuralNet(dims, haf, oaf, cfunc, lr, iwr, opt, ds, cfrac, vfrac, vint, tfrac, mbs,
    steps, mlay, mden, dweights, dbiases)
    operators = net.output
    grabbed_vars = net.hidden_layers
    inputs = net.input_features
    target = net.target
    inputData = np.array([[0,0],[0,1], [1,1], [1, 0]])
    targets = np.array([[0], [1], [0], [1]])
    feeder = {inputs: inputData, target: targets }

    for i in range(10000):
        res = net.run_one_step([operators], grabbed_vars, feed_dict=feeder)
        print("result:")
        for i in res:
            print(i)
        print("It probably ran successfully if this line is reached")

test_build()
